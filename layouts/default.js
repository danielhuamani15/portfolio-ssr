import Head from "next/head"
import Header from "../components/header"

const LayoutDefault = (props) => (
  <div className="">
    <Header></Header>
    <main>{props.children}</main>
  </div>
)

export default LayoutDefault
