import { NextSeo } from "next-seo"
import "../styles/normalize.css"
import "../styles/index.css"
import "../styles/fonts.css"

export default function App({ Component, pageProps }) {
  return (
    <>
      <NextSeo title="Simple Usage Example" />
      <Component {...pageProps} />
    </>
  )
}
