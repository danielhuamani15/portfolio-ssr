import Head from "next/head"
import Layout from "../layouts/default"
import styles from "../styles/Home.module.css"
import Article from "../components/article"
import Course from "../components/course"
export default function HomePage() {
  const listArticles = [1, 2, 3, 4, 5, 6, 7, 8, 9]
  const listCourse = [1, 2, 3, 4, 5, 6, 7, 8]
  return (
    <Layout>
      <div className="container mx-auto">
        <section className="mt-12">
          <h2 className="text-2xl">Últimos Articulos</h2>
          <div className="mt-6 grid gap-4 grid-cols-3">
            {listArticles.map((item) => {
              return <Article key={item}></Article>
            })}
          </div>
        </section>
        <section className="mt-12">
          <h2 className="text-2xl">Últimos Cursos</h2>
          <div className="mt-6 grid gap-5 grid-cols-4">
            {listCourse.map((item) => {
              return <Course key={item}></Course>
            })}
          </div>
        </section>
      </div>
    </Layout>
  )
}
