const theme = {
  colors: {
    white: "#fff",
    black: "#000",
    main: "#006DFF",
  },
  fonts: {
    bold: "cera_probold",
    medium: "cera_promedium",
    prothin: "cera_prothin",
  },
}
export default theme
