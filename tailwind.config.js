module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: ["./pages/**/*.js", "./components/**/*.js"],
  theme: {
    fontFamily: {
      regular: ["cera_proregular"],
      bold: ["cera_probold"],
      medium: ["cera_promedium"],
      prothin: ["cera_prothin"],
    },

    extend: {
      colors: {
        main: "#006DFF",
        light: "#679EFF",
      },
    },
  },
  variants: {},
  plugins: [],
}
