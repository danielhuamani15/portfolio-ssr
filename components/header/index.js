const HeaderMenu = () => {
  return (
    <header className="bg-white py-2 shadow-sm">
      <div className="container mx-auto flex justify-between items-center">
        <img src="/logo.png" width="60" />
        <nav>
          <ul className="flex">
            <li>
              <a className="cursor-pointer mr-4 font-bold text-base hover:text-main">
                Inicio
              </a>
            </li>
            <li>
              <a className="cursor-pointer mx-4 font-bold text-base hover:text-main">
                Articulos
              </a>
            </li>
            <li>
              <a className="cursor-pointer mx-4 font-bold text-base hover:text-main">
                Cursos
              </a>
            </li>
            <li>
              <a className="cursor-pointer ml-4 font-bold text-base hover:text-main">
                Acerca de mí
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  )
}

export default HeaderMenu
