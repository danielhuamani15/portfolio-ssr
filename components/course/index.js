import Tag from "../tag"
const Course = () => {
  return (
    <div className="max-w-sm rounded overflow-hidden shadow-sm bg-white">
      <img
        className="w-full"
        src="https://cdn.swapps.com/uploads/2019/04/django-faster.jpg"
        alt="Sunset in the mountains"
      />

      <div className="px-4 py-4">
        <div className="font-bold text-xl mb-2">The Coldest Sunset</div>
        <p className="text-gray-700 ">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus
          quia, nulla! Maiores et perferendis eaque, exercitationem praesentium
          nihil.
        </p>
      </div>
      <div className="px-4 pt-4 pb-2">
        <Tag title="#django"></Tag>
        <Tag title="#python"></Tag>
        <Tag title="#vue"></Tag>
      </div>
    </div>
  )
}
export default Course
