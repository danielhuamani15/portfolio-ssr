const Article = () => {
  return (
    <div className="bg-white rounded-lg p-4 border border-black">
      <h3 className="font-medium mb-2">Iniciando en Django</h3>
      <p className="mb-3 text-gray-700">Veremos como iniciar</p>
      <div className="flex justify-between">
        <div className="flex">
          <span className="bg-blue-300 rounded p-1 text-xs mr-1">Django</span>
          <span className="bg-blue-300 rounded p-1 text-xs">Python</span>
        </div>
        <p className="m-0 text-sm">15/08/2020</p>
      </div>
    </div>
  )
}

export default Article
