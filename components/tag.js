const Tag = (props) => {
  return (
    <span className="inline-block text-white bg-light rounded-full px-3 py-1  font-semibold text-gray-100 mr-2 mb-2 text-xs">
      {props.title}
    </span>
  )
}

export default Tag
